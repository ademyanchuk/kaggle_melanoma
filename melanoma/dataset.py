import albumentations as A
import cv2
import numpy as np
import skimage.io
from chestxray.datasets import TV_MEAN, TV_STD
from melanoma.config import CFG
from torch.utils.data import Dataset

# Augmentations

CUT_SIZE = int(CFG.rescale_sz * CFG.cutout_pct)

augs_dict = {
    "heavy": A.Compose(
        [
            A.RandomRotate90(),
            A.Flip(),
            A.OneOf(
                [
                    A.RandomBrightnessContrast(
                        brightness_limit=0.2, contrast_limit=0.2
                    ),
                    A.HueSaturationValue(
                        hue_shift_limit=20, sat_shift_limit=50, val_shift_limit=50
                    ),
                ]
            ),
            A.OneOf([A.IAAAdditiveGaussianNoise(), A.GaussNoise()]),
            A.OneOf(
                [
                    A.MotionBlur(p=0.2),
                    A.MedianBlur(blur_limit=3, p=0.1),
                    A.Blur(blur_limit=3, p=0.1),
                ]
            ),
            A.ShiftScaleRotate(shift_limit=0.1, scale_limit=0.1, rotate_limit=15, border_mode=0, p=0.85),
            # A.OneOf(
            #     [
            #         A.OpticalDistortion(p=0.3),
            #         A.GridDistortion(p=0.1),
            #         A.IAAPiecewiseAffine(p=0.3),
            #     ]
            # ),
            A.Resize(CFG.rescale_sz, CFG.rescale_sz),
            A.Cutout(num_holes=1, max_h_size=CUT_SIZE, max_w_size=CUT_SIZE),
        ]
    ),
    "light": A.Compose(
        [
            A.Flip(),
            A.ShiftScaleRotate(
                shift_limit=0.1,
                scale_limit=0.1,
                rotate_limit=10,
                border_mode=cv2.BORDER_REFLECT_101,
            ),
            A.HueSaturationValue(hue_shift_limit=10, sat_shift_limit=10),
            # A.RandomBrightnessContrast(brightness_limit=0.2, contrast_limit=0.2),
            # This transformation first / 255. -> scale to [0,1] and
            # then - mean and / by std
            # A.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225],),
            # # Convert to torch tensor and swap axis to make Chanel first
            # ToTensorV2(),
        ]
    ),
}


def get_transforms(*, data, aug="light"):
    """Choose mode `train` or `valid` and aug type:
    `light` or `heavy`"""

    assert data in ("train", "valid")
    assert aug in ("light", "heavy")

    if data == "train":
        return augs_dict[aug]

    elif data == "valid":
        return augs_dict[aug]


# Datasets

normalize = A.Normalize(mean=TV_MEAN, std=TV_STD)


def rescale(sz: int, type_: str) -> A.Compose:
    """Set of transforms for resize
    to square and preserve aspect ratio"""
    assert type_ in ["pad", "crop"]
    if type_ == "crop":
        return A.Compose([A.SmallestMaxSize(sz), A.CenterCrop(sz, sz)])
    else:
        return A.Compose([A.LongestMaxSize(sz), A.PadIfNeeded(sz, sz)])


class MelanomaDataset(Dataset):
    def __init__(
        self,
        df,
        imgs_path,
        target_col,
        id_col,
        is_train=True,
        rescale_type="pad",
        to_size=256,
        suffix="jpg",
        augment=None,
        debug=False,
    ):
        self.df = df
        self.imgs_path = imgs_path
        self.is_train = is_train
        if self.is_train:
            self.labels = df[target_col].values
        self.id_col = id_col
        self.suffix = suffix
        self.augment = augment
        self.debug = debug

        self.rescale = None
        if rescale_type:
            self.rescale = rescale(to_size, rescale_type)  # it is slow!

    def __len__(self):
        return len(self.df)

    def __getitem__(self, idx):
        data = {}

        file_id = self.df[self.id_col].values[idx]
        file_path = f"{self.imgs_path}/{file_id}.{self.suffix}"

        image = skimage.io.imread(file_path)
        if self.rescale is not None:
            image = self.rescale(image=image)["image"]

        if self.augment:
            augmented = self.augment(image=image)
            image = augmented["image"]

        image = normalize(image=image)["image"]
        image = image.transpose(2, 0, 1)  # to Chanel first

        if self.is_train:
            label = np.zeros(1).astype(np.float32)
            label[0] = self.labels[idx]
            data["label"] = label

        data["image"] = image
        if self.debug:
            data["file_id"] = file_id

        return data
