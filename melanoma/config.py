"""Kaggle oriented config"""

import os
from pathlib import Path

DATA_PATH = Path("/kaggle/input")

PROJ_PATH = Path("/kaggle/working")

MODEL_PATH = PROJ_PATH / "models"
if not MODEL_PATH.exists():
    MODEL_PATH.mkdir()


# Melanoma competition config
class CFG:
    # overall
    debug = False
    seed = 1982
    metric = "roc_auc"
    # misc
    smooth_label = False
    ls_value = 0.05
    # data
    ext_M = "M1+M2" # use external malignant "M1", "M2", "M1+M2", False
    ext_B = True  # if use external benign data
    ext_B_ratio = 4.0  # ratio of ext_B / ext_M counts
    ext_2019 = False  # if use external 2019 data at all
    img_path_suffix = "768"
    rescale_type = ""  # "pad", "crop" or empty string
    rescale_sz = 384
    target_size = 1
    img_id_col = "image_name"
    target_col = "target"
    group_col = "patient_id"
    use_sampler = False
    cls_weight_beta = 0.999
    base_bs = 24  # use in scaling lr with bs
    batch_size = 24
    accum_step = 6  # effective batch size will be batch_size * accum_step
    aug_type = "heavy"  # "light" or "heavy"
    # augs args
    cutout_pct = 0.35  # max size of cutout square as a percent of img size
    # model
    use_timm = True
    timm_global_pool = "catavgmax"  # see timm package for details
    drop_rate = 0.0  # classifier dropout
    arch = "tf_efficientnet_b5_ns"  # "resnet34", "resnet50" and timm models
    finetune = False  # or "1stage"
    head_type = "one_layer"  # "one_layer" or "deep"
    att = False
    # loss
    ohem = False  # will work with ohem and bce
    loss = "bce"  # "bce", "focal"
    loss_pw_beta = 0.5  # 0 or float in range (0, 1]
    # optim
    optim = "adamw"  # "adamw", "sgd" or "radam"
    base_lr = 4e-5  # use in scaling lr with bs
    lr = 0.001 if optim == "sgd" else ((batch_size / base_bs) * base_lr) * accum_step
    wd = 0.
    # schedule
    schedule_type = "cawr"  # "one_cycle", "reduce_on_plateau" or "cawr"
    oc_final_div_factor = 1e1
    cawr_T_0 = 12  # epochs untill first restart
    cawr_T_mult = 2  # multiply next restarts
    cawr_T_up = 1  # warmup epochs
    cawr_gamma = 0.7
    rlopp = 1  # learnig rate on plateu scheduler patience
    # training
    use_validation = True
    tta = 3  # number of tta rounds
    tta_train = 3  # tta applyed on every validation step
    resume = False
    epoch = 10
    n_fold = 5 # # of validation fold
    use_amp = True
    descript = "bce+pw tf_efficientnet_b5_ns 768->384 augs_v3 tta3 extB/extM=4 no 2019 OS 2020 positives"


JPG_IMGS = DATA_PATH / f"jpeg-melanoma-{CFG.img_path_suffix}x{CFG.img_path_suffix}"
TRAIN_JPG = JPG_IMGS / "train"
TEST_JPG = JPG_IMGS / "test"

EXT_M_JPG_IMGS = DATA_PATH / f"malignant-v2-{CFG.img_path_suffix}x{CFG.img_path_suffix}"
EXT_OLD_IMGS = DATA_PATH / f"jpeg-isic2019-{CFG.img_path_suffix}x{CFG.img_path_suffix}"

TRAIN_CSV = JPG_IMGS / "train.csv"
TEST_CSV = JPG_IMGS / "test.csv"
SAMPLE_CSV = JPG_IMGS / "sample_submission.csv"
