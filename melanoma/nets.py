from collections import OrderedDict

import timm
import torch
import torch.nn as nn
from efficientnet_pytorch import EfficientNet
from torchvision import models


class ClassifHead(nn.Module):
    def __init__(self, head_type, f_in, f_out):
        super().__init__()
        assert head_type in ["one_layer", "deep"]

        if head_type == "deep":
            self.head = nn.Sequential(
                OrderedDict(
                    [
                        ("cls_bn", nn.BatchNorm1d(f_in)),
                        ("cls_do", nn.Dropout()),
                        ("cls_fc", nn.Linear(f_in, 512)),
                        ("cls_relu", nn.ReLU(inplace=True)),
                        ("cls_logit", nn.Linear(512, f_out)),
                    ]
                )
            )
        elif head_type == "one_layer":
            self.head = nn.Sequential(
                OrderedDict([("cls_logit", nn.Linear(f_in, f_out))])
            )
        else:
            raise NotImplementedError

    def forward(self, x):
        return self.head(x)


class CNNEncoder(nn.Module):
    def __init__(self, arch, pretrained):
        super().__init__()

        implemented_archs = [
            "resnet34",
            "resnet50",
            "efficientnet-b0",
            "efficientnet-b3",
        ]

        assert isinstance(
            pretrained, bool
        ), f"pretrained should be bool, get {type(pretrained)}"
        assert (
            arch in implemented_archs
        ), f"Arch should be in {implemented_archs}, get {arch}"

        self.resnet_dict = {
            "resnet50": models.resnet50,
            "resnet34": models.resnet34,
        }
        self.effnet_tup = ("efficientnet-b0", "efficientnet-b3")

        self.arch = arch
        self.pretrained = pretrained

        self.encoder, self.out_size = self._make_encoder()

    def _make_encoder(self):
        if self.arch in self.resnet_dict:
            model = self.resnet_dict[self.arch](pretrained=self.pretrained)
            encoder = nn.Sequential(*list(model.children())[:-2])
            out_size = list(model.children())[-1].in_features
            del model
        elif self.arch in self.effnet_tup:
            if self.pretrained:
                encoder = EfficientNet.from_pretrained(self.arch)
            else:
                encoder = EfficientNet.from_name(self.arch)
            # not sure if needed (don't wont these layers)
            encoder._avg_pooling = nn.Identity()
            encoder._dropout = nn.Identity()
            encoder._fc = nn.Identity()
            out_size = encoder._bn1.num_features

        return encoder, out_size

    def forward(self, x):
        if self.arch in self.resnet_dict:
            return self.encoder(x)
        elif self.arch in self.effnet_tup:
            return self.encoder.extract_features(x)


class RNModel(nn.Module):
    def __init__(self, f_out, head_type, arch="resnet50", pretrained=True):
        super().__init__()

        self.encoder = CNNEncoder(arch, pretrained)
        feat_n = self.encoder.out_size * 2  # use concat pooling

        self.avgpool = nn.AdaptiveAvgPool2d(output_size=(1, 1))
        self.maxpool = nn.AdaptiveMaxPool2d(output_size=(1, 1))

        self.head = ClassifHead(head_type, feat_n, f_out)

    def forward(self, x):
        batch_size = x.shape[0]
        x = self.encoder(x)  # x -> bs x C(Maps) x H(Maps) x W(Maps)
        avg_x = self.avgpool(x)
        max_x = self.maxpool(x)
        x = torch.cat([avg_x, max_x], dim=1).view(batch_size, -1)
        x = self.head(x)
        return x


def model_from_timm(f_out, arch, pretrained, global_pool, drop_rate):
    m = timm.create_model(arch, pretrained=pretrained, drop_rate=drop_rate)
    m.reset_classifier(f_out, global_pool=global_pool)
    return m
